var http = require('http').Server(handler).listen(1337),
	io = require('socket.io').listen(http);

function handler(req, res) {
	res.writeHead(200);
	res.end('No Direct Access Allowed');
}

io.sockets.on('connection', function(socket) {
	socket.on('send', function(data) {
		socket.broadcast.send(JSON.stringify({name: data.name, message: data.message }));
	});
});