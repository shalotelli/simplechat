<html>
	<head>
		<meta charset="utf-8">
		<title>Socket.IO</title>
		<script type="text/javascript" src="http://<?php echo $_SERVER['SERVER_ADDR']; ?>:1337/socket.io/socket.io.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

		<script type="text/javascript">
			$(function() {
				var socket = io.connect("http://<?php echo $_SERVER['SERVER_ADDR']; ?>:1337"),
					$log = $("#message_log"),
					$message = $("#message"),
					$name = $("#name");

				$("#send").on('click', function() {
					$log.append('<b>Me: </b>'+$message.val()+'<br />');

					socket.emit('send', { name: $name.val(), message: $message.val() });
				});

				socket.on('message', function(data) {
					data = JSON.parse(data);
					$log.append('<b>'+data.name+'</b>: '+data.message+'<br />');
				});
			});
		</script>
	</head>
	<body>
		<div>name<input type="text" id="name"></div>
		<div>message<textarea name="" id="message" cols="30" rows="10"></textarea></div>
		<div><button id="send">Send</button></div>

		<hr size="1">

		<div id="message_log" style="width:40%;border:1px solid #000;"></div>
	</body>
</html>